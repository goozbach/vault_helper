#!/bin/bash

echo "1..9"

COUNT=0

getwhich() {
  return $(which ${1} > /dev/null)
}

checkpackage() {
  ((COUNT=COUNT+1))
  getwhich ${1} && echo "ok ${COUNT} - Package ${1} installed" || echo "not ok ${COUNT} - Package ${1} installed"
}

checkpackage s3cmd
checkpackage stow
checkpackage git
checkpackage jq
checkpackage curl
checkpackage tar
checkpackage unzip
checkpackage gpg
checkpackage vault

