#!/bin/bash
set -o pipefail

echo "1..11"

export GPG_TTY=$(tty)

diag() {
    echo "${1}" | while read line; do echo "# ${2} $line"; done
}

echo -n "# Starting vault server... "
vault server -config=/opt/vault_helper/testing/vault.config -log-level=debug -log-format=json &> /var/log/vault-debug.log &
VAULTPID=$!
disown
sleep 3
echo "DONE VAULTPID: ${VAULTPID}"

_02-cleanup(){
    if [[ "${VH_NOCLEAN}" == "true" ]]
    then
      echo "# not cleaning up"
    else
      echo "# Running Cleanup Tasks"
      echo "#	Killing vault server ${VAULTPID}"
      kill ${VAULTPID}
      echo "#	killing gpg-agent"
      gpgconf --kill gpg-agent
      echo "#	rm -rf /etc/gpg-pass"
      rm -rf /etc/gpg-pass
      echo "#	rm -rf ~/.gnupg"
      rm -rf ~/.gnupg
      echo "#	rm -rf /var/lib/vault/*"
      rm -rf /var/lib/vault/*
      echo "#	rm -rf /var/tmp/vault_helper/*"
      rm -rf /var/tmp/vault_helper/*
      echo "#	rm -rf /tmp/signtest*"
      rm -rf /tmp/signtest*
      echo "#	rm -rf /tmp/vault_export*"
      rm -rf /tmp/vault_export*
      echo "# End of Cleanup Tasks"
    fi
}

trap "_02-cleanup" EXIT

export VAULT_ADDR='http://localhost:8200'

# 01 is vault running
VAULTSTATUS=$(vault status -format=json)
VAULTRC=$?

if [[ ${VAULTRC} == 2 ]]
then
  if [[ $(vault status -format=json | jq -r '.initialized') == 'false' ]]
  then
    echo "ok 1 - Vault is running, sealed, and NOT initialized"
  else
    echo "Bail out! Vault is running, sealed, and IS initialized"
    exit 1
  fi
else
  echo "not ok 1 - Vault is running and sealed"
fi  

# 02 run vault_helper init
# create vault helper config
mkdir -p /etc/vault_helper
ln -sf /opt/vault_helper/testing/config /etc/vault_helper/config

mkdir -p ~/.gnupg
chmod 0700 ~/.gnupg
cp -nf /opt/vault_helper/testing/gpg/gpg-agent.conf ~/.gnupg/gpg-agent.conf
cp -nf /opt/vault_helper/testing/gpg/gpg.conf ~/.gnupg/gpg.conf
gpg-connect-agent reloadagent /bye &>/dev/null

echo "cangetin" > /etc/gpg-pass
gpg --batch --use-agent --gen-key /opt/vault_helper/testing/keys.generate &> /var/log/gpg-testing.log
for i in foo bar root; do gpg --export -a "${i}@example.com" > /opt/${i}.gpg; done

vault_helper init | while read line; do echo "# vault_helper init output: $line"; done
HELPERRC=$?

if [[ ${HELPERRC} == 0 ]]
then
  echo "ok 2 - vault_helper init returncode"
else
  echo "not ok 2 - vault_helper init returncode"
fi

# 03 vault_helper/init.vault is json file with proper keys
if [[ $(jq -r '.unseal_keys_b64 | length' /var/tmp/vault_helper/init.vault) == 2 && $(jq -r '.unseal_shares' /var/tmp/vault_helper/init.vault) == 2 ]]
then
  echo "ok 3 - init file is provisionally valid"
else
  echo "not ok 3 - init file is not provisionally valid"
fi

# 04 vault server should be initialized now
# 01 is vault running
VAULTSTATUS=$(vault status -format=json)
VAULTRC=$?

if [[ ${VAULTRC} == 2 ]]
then
  if [[ $(vault status -format=json | jq -r '.initialized') == 'true' ]]
  then
    echo "ok 4 - Vault is running, sealed, and initialized"
  else
    echo "not ok 4 - Vault is running, sealed, and is NOT initialized"
  fi
else
  echo "not ok 4 - Vault is running, sealed and initialized"
fi  

# 05 test export
rm -rf /tmp/signtest*
echo "testifle" > /tmp/signtest
echo "cangetin" | gpg -a --batch --use-agent --passphrase-file /etc/gpg-pass --detach-sig /tmp/signtest

vault_helper export 2>&1 | while read line; do echo "# vault_helper export output: $line"; done
VH_EXPORT_RC=$?

if [[ ${VH_EXPORT_RC} == 0 ]]
then
  echo "ok 5 - Vault Helper export returncode is 0"
else
  echo "not ok 5 - Vault Helper export returncode is NOT 0"
fi

# 06 verify that the tar balls that exist at /tmp/vault_export*.tar.bz2 and is a tarball
TARFAILURE=0
for tarball in /tmp/vault_export*.tar.bz2
do
  if tar --test-label -f ${tarball} &>/dev/null
  then
      echo "# Tarball ${tarball} is valid tarball"
  else
      echo "# Tarball ${tarball} is corrupt or not valid"
      TARFAILURE=1
  fi
done
if [[ ${TARFAILURE} == 0 ]]
then
  echo "ok 6 - Tarball output is valid tarball"
else
  echo "not ok 6 - Tarball output is not valid tarball"
fi

# 07 verify that tar ball signature is valid 
VERIFYFAILURE=0
for signature in /tmp/vault_export*.tar.bz2.asc
do
  VERIFYOUTPUT=$(gpg --verify ${signature} 2>&1)
  VERIFYRC=$?
  if [[ ${VERIFYRC} == 0 ]]
  then
      if echo ${VERIFYOUTPUT} | grep -q 'WARNING: not a detached signature'
      then
        diag "${VERIFYOUTPUT}" "gpg verify output:"
        echo "# Signature ${signature} is valid, but not detached"
        VERIFYFAILURE=1
      else
        echo "# Signature ${signature} is valid"
      fi
  else
      diag "${VERIFYOUTPUT}" "gpg verify output:"
      echo "# Signature ${signature} is NOT valid"
      VERIFYFAILURE=1
  fi
done
if [[ ${VERIFYFAILURE} == 0 ]]
then
  echo "ok 7 - Signature of tarball is valid"
else
  echo "no ok 7 - Signature of tarball is not valid"
fi

# 08 verify that an additional export doesn't return error
BEFORE_SHARDCOUNT=$(ls -1 /var/tmp/vault_helper/export/init/*.asc | wc -l)
BEFORE_EXPORTCOUNT=$(ls -1 /tmp/vault_export* | wc -l)

vault_helper export 2>&1 | while read line; do echo "# vault_helper export take two output: $line"; done
VH_EXPORT_RC=$?

if [[ ${VH_EXPORT_RC} == 0 ]]
then
  echo "ok 8 - Vault Helper export take two returncode is 0"
else
  echo "not ok 8 - Vault Helper export take two returncode is NOT 0"
fi

# 09 verify there aren't double exports anymore
AFTER_SHARDCOUNT=$(ls -1 /var/tmp/vault_helper/export/init/*.asc | wc -l)
AFTER_EXPORTCOUNT=$(ls -1 /tmp/vault_export* | wc -l)

if [[ ${BEFORE_SHARDCOUNT} -eq ${AFTER_SHARDCOUNT} && ${BEFORE_EXPORTCOUNT} -eq ${AFTER_EXPORTCOUNT} ]]
then
  echo "ok 9 - Before and after shardcounts match"
else
  echo "not ok 9 - Before and after shardcounts do not match"
fi

# 10 shard has env_name
ENVNAME_FAILURE=0
for shard in /var/tmp/vault_helper/export/init/*.asc
do
  if echo ${shard} | grep -q '\.testing\.' 
  then
    diag "shard ${shard} has 'testing' env name"
  else
    diag "shard ${shard} does not have 'testing' env name"
    ENVNAME_FAILURE=1
  fi
done
if [[ ENVNAME_FAILURE -eq 0 ]]
then
  echo "ok 10 - Shard filename contains ENV_NAME"
else
  echo "not ok 10 - Shard filename does not contain ENV_NAME"
fi

# 11 TODO verify that unseal keys can unseal
for shard in $(jq -r '.unseal_keys_b64[]' /var/tmp/vault_helper/init.vault)
do
  vault operator unseal $(echo ${shard} 2>/dev/null | base64 -d | gpg --batch --use-agent  --passphrase-file /etc/gpg-pass -d 2> /dev/null; echo)
done

VAULTSTATUS3=$(vault status -format=json)
VAULTRC3=$?

if [[ ${VAULTRC3} == 0 ]]
then
  if [[ $(echo ${VAULTSTATUS3} | jq -r '.sealed') == "false" ]]
  then
    echo "ok 11 - shards can unseal, vault is unsealed"
  else
    echo "not ok 11 - shards can not unseal, vault is still sealed"
    diag "${VAULTSTATUS3}"
  fi
else
  echo "not ok 11 - shards can unseal -- status failure"
  diag "${VAULTSTATUS3}"
fi

