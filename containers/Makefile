.PHONY: fedora centos ubuntu all fedora-clean centos-clean ubuntu-clean clean push nuke vault-clean

VAULT_VERSION ?= $(shell curl -s https://api.github.com/repos/hashicorp/vault/tags | jq -r '.[0].name' | sed -e s/^v//)
all: fedora centos ubuntu

clean: vault-clean fedora-clean centos-clean ubuntu-clean

vault-clean:
	rm -rf vault.zip vault 
	rm -rf fedora/vault centos/vault ubuntu/vault

# only needs to be ran once
nuke: vault-clean
	cd fedora && $(MAKE) nuke

push: fedora-push centos-push ubuntu-push

vault.zip: Makefile
	@ echo vault_version=$(VAULT_VERSION)
	curl -sLo vault.zip https://releases.hashicorp.com/vault/$(VAULT_VERSION)/vault_$(VAULT_VERSION)_linux_amd64.zip

vault: vault.zip
	unzip -u vault.zip

fedora/vault: vault
	cp vault fedora

centos/vault: vault
	cp vault centos

ubuntu/vault: vault
	cp vault ubuntu

fedora: fedora/vault
	cd fedora && $(MAKE)

centos: centos/vault
	cd centos && $(MAKE)

ubuntu: ubuntu/vault
	cd ubuntu && $(MAKE)

fedora-clean:
	cd fedora && $(MAKE) clean

centos-clean:
	cd centos && $(MAKE) clean

ubuntu-clean:
	cd ubuntu && $(MAKE) clean

fedora-push: fedora/vault
	cd fedora && $(MAKE) push

centos-push: centos/vault
	cd centos && $(MAKE) push

ubuntu-push: ubuntu/vault
	cd ubuntu && $(MAKE) push
